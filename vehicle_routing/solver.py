#!/usr/bin/python
# -*- coding: utf-8 -*-

import math
from collections import namedtuple
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp

"""
Using google's ortools for the vehicle routing problem. Using default ortools initial solution selection
and guided local search for an allowable time proportional to the number of customers to be served.
"""

Customer = namedtuple("Customer", ["index", "demand", "x", "y"])


def length(customer1, customer2):
    return math.sqrt(
        (customer1.x - customer2.x) ** 2 + (customer1.y - customer2.y) ** 2
    )


def print_solution(vehicle_count, customers, manager, routing, assignment):
    """Prints assignment on console."""
    total_distance = 0
    total_load = 0
    vehicle_routes = []
    for vehicle_id in range(vehicle_count):
        index = routing.Start(vehicle_id)
        plan_output = "Route for vehicle {}:\n".format(vehicle_id)
        route_distance = 0
        route_load = 0
        # Getting the route for the vehicle
        temp_route = []
        while not routing.IsEnd(index):
            node_index = manager.IndexToNode(index)
            route_load += customers[node_index].demand
            plan_output += " {0} Load({1}) -> ".format(node_index, route_load)
            previous_index = index
            index = assignment.Value(routing.NextVar(index))
            temp_route.append(node_index)
            route_distance += routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id
            )
        temp_route.append(0)
        plan_output += " {0} Load({1})\n".format(manager.IndexToNode(index), route_load)
        plan_output += "Distance of the route: {}m\n".format(route_distance)
        plan_output += "Load of the route: {}\n".format(route_load)
        print(plan_output)
        total_distance += route_distance
        total_load += route_load
        vehicle_routes.append(temp_route)
    print("Total distance of all routes: {}m".format(total_distance))
    print("Total load of all routes: {}".format(total_load))
    return (total_distance, vehicle_routes)


def solve_it(input_data):
    # Modify this code to run your optimization algorithm

    # parse the input
    lines = input_data.split("\n")

    parts = lines[0].split()
    customer_count = int(parts[0])
    vehicle_count = int(parts[1])
    vehicle_capacity = int(parts[2])

    customers = []
    for i in range(1, customer_count + 1):
        line = lines[i]
        parts = line.split()
        customers.append(
            Customer(i - 1, int(parts[0]), float(parts[1]), float(parts[2]))
        )

    # the depot is always the first customer in the input
    depot = customers[0]

    # Build the distance matrix
    dist_mat = []
    for i in range(customer_count):
        dist_vec_temp = [float("inf")] * customer_count
        for j in range(customer_count):
            dist_vec_temp[j] = length(customers[i], customers[j])
        dist_mat.append(dist_vec_temp)

    # Create and register a transit callback.
    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return dist_mat[from_node][to_node]

    # Build the demand vector
    demands = []
    for customer in customers:
        demands.append(customer.demand)

    # Build the vehicle capacity vector
    capacities = []
    for i in range(vehicle_count):
        capacities.append(vehicle_capacity)

    manager = pywrapcp.RoutingIndexManager(
        customer_count, vehicle_count, 0
    )  # The depot is 0

    routing = pywrapcp.RoutingModel(manager)

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    # State the cost of the arcs
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Define the capacity constraint
    # Add Capacity constraint.
    def demand_callback(from_index):
        """Returns the demand of the node."""
        # Convert from routing variable Index to demands NodeIndex.
        from_node = manager.IndexToNode(from_index)
        return demands[from_node]

    demand_callback_index = routing.RegisterUnaryTransitCallback(demand_callback)
    routing.AddDimensionWithVehicleCapacity(
        demand_callback_index,
        0,  # null capacity slack
        capacities,  # vehicle maximum capacities
        True,  # start cumul to zero
        "Capacity",
    )

    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.AUTOMATIC
    )
    search_parameters.local_search_metaheuristic = (
        routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH
    )
    search_parameters.time_limit.seconds = int(customer_count)
    search_parameters.log_search = True

    # Solve the problem.
    assignment = routing.SolveWithParameters(search_parameters)

    # Print solution on console.
    if assignment:
        obj, routes = print_solution(
            vehicle_count, customers, manager, routing, assignment
        )
    obj = 0
    for vehicle_tour in routes:
        if len(vehicle_tour) > 0:
            obj += length(depot, customers[vehicle_tour[1]])
            for i in range(1, len(vehicle_tour) - 1):
                obj += length(
                    customers[vehicle_tour[i]], customers[vehicle_tour[i + 1]]
                )
    # prepare the solution in the specified output format
    outputData = "%.2f" % obj + " " + str(0) + "\n"
    for route in routes:
        outputData += " ".join([str(loc) for loc in route]) + "\n"
    return outputData


import sys

if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, "r") as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:

        print(
            "This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/vrp_5_4_1)"
        )
