#!/usr/bin/python
# -*- coding: utf-8 -*-
import pyomo.environ as pyomo
import os
import numpy as np

"""
Solving the graph coloring problem using pyomo

"""


def solve_it(input_data):

    # For Debugging:
    # input_data = open("Hw_3/coloring/data/gc_20_1").read()

    lines = input_data.split("\n")

    first_line = lines[0].split()
    node_count = int(first_line[0])
    edge_count = int(first_line[1])
    opt = 0
    edges = []
    for i in range(1, edge_count + 1):
        line = lines[i]
        parts = line.split()
        edges.append((int(parts[0]), int(parts[1])))

    # Build a MIP formulation

    # Define the model
    model = pyomo.ConcreteModel()

    # Define the constants
    model.nodes = range(node_count)
    model.edges = range(edge_count)

    # Define the decision variable -- A binary variables indicating the node and color.
    # Ex. node_color[5,3] == 1 means node 5 is color 3
    model.node_color = pyomo.Var(model.nodes, model.nodes, within=pyomo.Binary)

    # Define the constraint -- a node can only be one color
    model.single_color_constraint = pyomo.ConstraintList()
    for i in model.nodes:
        model.single_color_constraint.add(
            sum([model.node_color[i, j] for j in model.nodes]) == 1
        )

    # Define the constraint -- no node connected by an edge may be the same color
    model.edge_constraint = pyomo.ConstraintList()
    for i in model.edges:
        u = edges[i][0]
        w = edges[i][1]
        for c in model.nodes:
            model.edge_constraint.add(
                model.node_color[u, c] + model.node_color[w, c] <= 1
            )

    # Define the objecitve function, minimize the number of colors used
    model.obj = pyomo.Objective(
        expr=np.sum(
            [
                np.sum([model.node_color[i, c] * (c + 1) for i in model.nodes])
                for c in model.nodes
            ]
        ),
        sense=pyomo.minimize,
    )

    solver = pyomo.SolverFactory("cbc")
    print("Solving the model")
    solver.solve(model)
    print("Model is solved")

    solution = []
    for i in range(node_count):
        for j in range(node_count):
            if model.node_color[i, j] == 1:
                solution.append(j)
                break

    obj = max(solution)
    # prepare the solution in the specified output format
    output_data = str(obj + 1) + " " + str(opt) + "\n"
    output_data += " ".join(map(str, solution))
    # print(output_data)
    # print(edges)
    return output_data


import sys

if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, "r") as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        # solve_it(open('data/gc_4_1').read())
        print(
            "This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/gc_4_1)"
        )
