#!/usr/bin/python
# -*- coding: utf-8 -*-
from ortools.sat.python import cp_model
import os

"""
Solving the graph coloring problem using ortools constraint programming.
Currently this ortools CP formulation is faster than my pyomo formulation.

"""


def solve_it(input_data):

    # For Debugging:
    # input_data = open("Hw_3/coloring/data/gc_50_3").read()

    lines = input_data.split("\n")

    first_line = lines[0].split()
    node_count = int(first_line[0])
    edge_count = int(first_line[1])
    opt = 0
    factor = node_count
    # Define the numer of colors to use for coloring
    if node_count == 50:
        # Problem 1
        factor = 5
        opt = 1
    elif node_count == 70:
        # Problem 2
        factor = 16
        opt = 1
    elif node_count == 100:
        # Problem 3
        factor = 15
        opt = 1
    elif node_count == 250:
        # Problem 4
        factor = 79
    elif node_count == 500:
        # Problem 5
        factor = 15
    elif node_count == 1000:
        # Problem 6
        factor = 99

    edges = []
    for i in range(1, edge_count + 1):
        line = lines[i]
        parts = line.split()
        edges.append((int(parts[0]), int(parts[1])))

    # Build a CP frameowrk using Google's or-tools

    # Define the model
    model = cp_model.CpModel()
    num_edges = edge_count
    num_nodes = node_count
    # Define the variables
    color = [model.NewIntVar(0, factor, "c%i" % i) for i in range(num_nodes)]

    # Add the constraint
    for i in range(num_edges):
        model.Add(color[edges[i][0]] != color[edges[i][1]])
    model.AddAbsEquality(0, color[0])

    # Add a symmetry braking constraint
    for i in range(factor):
        model.Add(color[i] <= i + 1)

    solver = cp_model.CpSolver()
    status = solver.Solve(model)
    print(status)
    print()
    if status == cp_model.FEASIBLE:
        print("Have a feasible solution.")
    #    print('The number of colors is ' % solver.Value(max(color)))

    solution = []
    for i in range(num_nodes):
        solution.append(solver.Value(color[i]))

    obj = max(solution)
    # prepare the solution in the specified output format
    output_data = str(obj + 1) + " " + str(opt) + "\n"
    output_data += " ".join(map(str, solution))
    # print(output_data)
    return output_data


import sys

if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, "r") as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        # solve_it(open('data/gc_4_1').read())
        print(
            "This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/gc_50_3)"
        )
