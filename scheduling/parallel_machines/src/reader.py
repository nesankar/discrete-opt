import sys
from collections import namedtuple
from dataclasses import dataclass, field
from pathlib import Path
import numpy as np
from typing import List


@dataclass
class Instance:
    machines: int
    jobs: List[int]
    total_time: float = field(init=False)

    def __post_init__(self):
        self.total_time = np.sum(self.jobs)
        self.average_time = self.total_time / self.machines
        self.lb = max(self.average_time, max(self.jobs))


def read_text_file(file_path: Path) -> Instance:
    """Read in a simple text file of a parallel machine problem. First line is |machines| |jobs|
    remaining lines are the job times.
    """
    job_times = []
    with open(file_path) as file:
        for i, line in enumerate(file):
            if i == 0:
                # This is the header "n_machines n_jobs"
                instance_data = line.strip().split(" ")
                n_machines = int(instance_data[0])
                n_jobs = int(instance_data[1])
            else:
                job_time = int(line.strip())
                job_times.append(job_time)
        assert (
            len(job_times) == n_jobs
        ), f"There are {len(job_times)} job times found, for {n_jobs} jobs."

    return Instance(n_machines, job_times)


if __name__ == "__main__":
    fp = Path.cwd() / "scheduling" / "parallel_machines" / "instances" / "p1.txt"
    print(read_text_file(fp))
