import numpy as np
import typing
from pathlib import Path
from tqdm import tqdm
from reader import read_text_file, Instance
from typing import Optional, Any, List
from ortools.sat.python import cp_model
from collections import namedtuple
import pandas as pd

solution = namedtuple("solution", ["objective", "solution"])


class Scheduler:
    """A class to perform list scheduling in a parallel machine environment"""

    def __init__(self, instance: Instance, constraints: Optional[Any] = None) -> None:
        self.instance = instance
        self.constraints = constraints

    def list_scheduler(
        self, jobs: Optional[List[int]], job_indecies: Optional[List[int]] = None
    ) -> solution:
        """Perform simple greedy LS on n_machines, return the makespan"""

        if not jobs:
            jobs = self.instance.jobs

        # Initialize the "time" on the machines
        machine_times = [0 for _ in range(self.instance.machines)]
        machine_jobs = [[] for _ in range(self.instance.machines)]

        # Iterate over the jobs, and place each one on the machine with the minimum "time" alloted
        for i, job in enumerate(jobs):
            # Get the machine with the least working time...
            min_machine = np.argmin(machine_times)

            # ... and assign it the current job.
            machine_times[min_machine] += job
            if job_indecies:
                machine_jobs[min_machine].append(job_indecies[i])
            else:
                machine_jobs[min_machine].append(i)

        # Get the total time each machine was working...
        makespan = max(machine_times)

        return solution(makespan, machine_jobs)

    def lpt_scheduler(self, jobs: Optional[List[int]]) -> solution:
        """Here, just sort the jobs and then call the list scheduler,
        but keep the original indecies of the list too.
        """
        if not jobs:
            jobs = self.instance.jobs
        indecies = list(range(len(jobs)))

        job_df = pd.DataFrame(jobs, index=indecies, columns=["job_time"])

        job_df = job_df.sort_values("job_time", ascending=False)

        sorted_job_times = list(job_df["job_time"])
        sorted_job_ids = list(job_df.index)

        return self.list_scheduler(jobs=sorted_job_times, job_indecies=sorted_job_ids)


    def cp_solver()


if __name__ == "__main__":
    files = ["p1.txt", "p2.txt", "p3.txt"]

    makespans = []
    lpts = []
    for i, file_name in enumerate(files):
        file_path = (
            Path.cwd() / "scheduling" / "parallel_machines" / "instances" / file_name
        )
        # read the file
        problem = read_text_file(file_path)
        # initialize the solver
        scheduling_solver = Scheduler(problem)
        # solve the problem
        makespans.append(scheduling_solver.list_scheduler(problem.jobs).objective)
        lpts.append(scheduling_solver.lpt_scheduler(problem.jobs).objective)
        print(f"The Optimal lower bound for job {i} is: {problem.lb}")
    print(f"The list scheduling solution makespans are: {makespans}")
    print(f"The lpt solution makespans are: {lpts}")
    with open(
        Path.cwd()
        / "scheduling"
        / "parallel_machines"
        / "solutions"
        / "ls_solutions.txt",
        "w+",
    ) as output_file:
        output_file.write(" ".join([str(val) for val in makespans]))
    with open(
        Path.cwd()
        / "scheduling"
        / "parallel_machines"
        / "solutions"
        / "lpt_solutions.txt",
        "w+",
    ) as output_file:
        output_file.write(" ".join([str(val) for val in lpts]))
