#!/usr/bin/python
# -*- coding: utf-8 -*-

import math
import numpy as np
import random
from collections import namedtuple
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp

"""
Submitting via solving the TSP using google ortools routing packages.
The methodology uses christofides algorithm to build a solution, and a
timed local search operator.

The TSP solutions are generally quite fast.

"""

Point = namedtuple("Point", ["x", "y"])


def length(point1, point2):
    return math.sqrt((point1.x - point2.x) ** 2 + (point1.y - point2.y) ** 2)


def solve_it(input_data):
    # Modify this code to run your optimization algorithm
    itts = 500
    # parse the input
    lines = input_data.split("\n")
    nodeCount = int(lines[0])
    if nodeCount > 33000:
        itts = 3600
    points = []
    for i in range(1, nodeCount + 1):
        line = lines[i]
        parts = line.split()
        points.append(Point(float(parts[0]), float(parts[1])))

    # Build a distance matrix, must be ints!:
    dist = []
    for i in range(len(points)):
        node_dist = []
        for j in range(len(points)):
            dist_temp = int(length(points[i], points[j]) * 100)
            node_dist.append(dist_temp)
        dist.append(node_dist)

    # Using Google or-tools to solve the problem:
    def create_data_model(distances):
        """Stores the data for the problem."""
        data = {}
        data["distance_matrix"] = distances  # yapf: disable
        data["num_vehicles"] = 1
        data["depot"] = 0
        return data

    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data["distance_matrix"][from_node][to_node]

    def print_solution(manager, routing, assignment):
        """Prints assignment on console."""
        print("Objective: {} miles".format(assignment.ObjectiveValue() / 100))
        index = routing.Start(0)
        plan_output = "Route for vehicle 0:\n"
        route_distance = 0
        tour = []
        while not routing.IsEnd(index):
            plan_output += " {} ->".format(manager.IndexToNode(index))
            tour.append(manager.IndexToNode(index))
            previous_index = index
            index = assignment.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(previous_index, index, 0)
        plan_output += " {}\n".format(manager.IndexToNode(index))
        print(plan_output)
        plan_output += "Route distance: {}miles\n".format(route_distance)
        return tour

    """Entry point of the program."""
    # Instantiate the data problem.
    data = create_data_model(dist)

    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(
        len(data["distance_matrix"]), data["num_vehicles"], data["depot"]
    )

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    # Define cost of each arc.
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.CHRISTOFIDES
    )
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.local_search_metaheuristic = (
        routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH
    )
    search_parameters.time_limit.seconds = 500
    search_parameters.log_search = True

    # Solve the problem.
    assignment = routing.SolveWithParameters(search_parameters)

    # Print solution on console.
    if assignment:
        output = print_solution(manager, routing, assignment)

    # calculate the length of the tour
    obj = length(points[output[-1]], points[output[0]])
    for index in range(0, nodeCount - 1):
        obj += length(points[output[index]], points[output[index + 1]])

    # prepare the solution in the specified output format
    output_data = "%.2f" % obj + " " + str(0) + "\n"
    output_data += " ".join(map(str, output))

    return output_data


import sys

if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, "r") as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        print(
            "This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/tsp_51_1)"
        )

"""

Previously used methods:

    1. Genetic Algorithm
    def initilize_solution(n_cities):
    sol=[i for i in range(n_cities)]
    random.shuffle(sol)
    return sol

def partially_matched_cx(mother,father):
    # 1. Randomly select a swath of alleles from parent 1 and copy them directly to the child.
    # Note the indexes of the segment.
    child=[-1]*(len(mother))
    completed=[i for i in range(0, len(mother))]
    p1=random.randint(0,len(mother)-2)
    p2=random.randint(p1+1,len(mother)-1)
    for i in range(p1,p2):
        child[i]=mother[i]
        completed.remove(i)

    # Place the values in the swath of father, NOT in child
    for i in range(p1,p2):
        val_check=father[i]
        if val_check not in child:
            match=mother[i]
            if match == 0:
                t=1
            father_match=father.index(match)
            while father_match in range(p1,p2):
                match=mother[father_match]
                father_match=father.index(match)
            child[father_match]=val_check
            completed.remove(father_match)

    for i in completed:
        child[i]=father[i]
    return child

def tournament(scores, players):
    best = float('inf')
    for player in players:
        if scores[player]<best:
            winner=player
            best=scores[player]
    return winner

def evaluation(sol, points):
    obj = length(points[sol[-1]], points[sol[0]])
    for i in range(1,len(sol)):
        obj += length(points[sol[i-1]], points[sol[i]])
    return obj

        # Build a GA for this problem
    POP_SIZE = 2000
    NGEN = 2000
    CROSSP = 0.8
    MUTP = 0.2
    N_PLAYERS = 2

    population=[]
    for i in range(POP_SIZE):
        temp=initilize_solution(nodeCount)
        population.append(temp)

    fitness=[]
    c=1
    for individual in population:
        score=evaluation(individual.copy(), points)
        if c%80==0:
            print('Evaluated %d percent of the population.'% int(c/len(population)*100))
        fitness.append(score)
        c=c+1

    best_sol=float('inf')
    for g in range(1,NGEN):
        # Select the next pairs of parents
        parents=[]
        for i in range(len(population)):
            t1=set()
            t2=set()
            while len(t1)<N_PLAYERS:
                t1.add(random.randint(0,len(population)-1))
            while len(t2)<N_PLAYERS:
                t2.add(random.randint(0,len(population)-1))
            p1=tournament(fitness,t1)
            p2=tournament(fitness,t2)
            parents.append([p1,p2])


        # Apply crossover on the offspring
        offspring=[]
        for pair in parents:
            cross_check=random.random()
            if cross_check<CROSSP:
                child=partially_matched_cx(population[pair[0]].copy(),population[pair[1]].copy())
                offspring.append(child)
            else:
                offspring.append(population[pair[0]])

        # Apply mutation to offspring
        for i in range(len(offspring)):
            mut_check=random.random()
            if mut_check<MUTP:
                # mutate
                child=offspring[i].copy()
                mut_inds=set()
                while len(mut_inds)<2:
                    mut_inds.add(random.randint(0,len(child)-1))
                mut_inds=list(mut_inds)
                v1=child[mut_inds[0]]
                child[mut_inds[0]]=child[mut_inds[1]]
                child[mut_inds[1]]=v1
                offspring[i]=child

        if g>1:
            offspring[0]=solution
        # Evaluate the individuals with an invalid fitness
        fitness=[]
        c=1
        for individual in offspring:
            score=evaluation(individual, points)
            # if c%20==0:
            #     print('Evaluated %d percent of the population.'% int(c/len(population)*100))
            fitness.append(score)
            c+=1

        if min(fitness)<best_sol:
            best_sol=min(fitness)
            solution=offspring[np.argmin(fitness)]
        if g%30 == 0:
            print("\nCompleted Generation #%d\n" % g)
            print("\nBest objective so far is: %f\n" % best_sol)
        population = offspring

    output = solution


"""
