#!/usr/bin/python
# -*- coding: utf-8 -*-

# Knapsack solver

from collections import namedtuple
from ortools.algorithms import pywrapknapsack_solver
import numpy as np
from tqdm import tqdm
from pathlib import Path

Item = namedtuple("Item", ["index", "value", "weight", "unit_value"])


def solve_it_dp(input_data):

    # parse the input
    lines = input_data.split("\n")

    firstLine = lines[0].split()
    item_count = int(firstLine[0])
    capacity = int(firstLine[1])

    items = []

    for i in range(1, item_count + 1):
        line = lines[i]
        parts = line.split()
        items.append(
            Item(i - 1, int(parts[0]), int(parts[1]), int(parts[0]) / int(parts[1]))
        )

    # goal here is to iterate over all items at each capcity level
    # First, ititialize the solution matrix
    current_sol = np.zeros((capacity + 1, item_count + 1))

    # Now iterate over the items
    for i, item in tqdm(enumerate(items)):
        # and over the capacities
        for j, current_capacity in enumerate(range(1, capacity + 1)):
            # NOTE: i rows, j cols and I don't need to do the first column
            if item.weight < current_capacity:
                current_sol[i, j] = max(
                    current_sol[i, j - item.weight],
                    item.value + current_sol[i, j - item.weight],
                )
            else:
                current_sol[i, j] = current_sol[i, j - 1]

    # Now current sol stores our optimal value, backtrack to get the solution
    iidx = len(items)
    jidx = capacity + 1
    solution = []
    while iidx > 0:
        current_pt = current_sol[iidx, jidx]
        if current_pt == current_sol[iidx, jidx - 1]:
            # This mens ass item iidx to the solution
            solution.append(items[iidx].index)
            jidx -= items[iidx].weight
            iidx -= 1
    bp = 1
    return solution


def solve_kpsk_dp(input_data):

    # parse the input
    lines = input_data.split("\n")

    firstLine = lines[0].split()
    item_count = int(firstLine[0])
    capacity = int(firstLine[1])

    items = []

    for i in range(1, item_count + 1):
        line = lines[i]
        parts = line.split()
        items.append(
            Item(i - 1, int(parts[0]), int(parts[1]), int(parts[0]) / int(parts[1]))
        )

    # First, generate an initial solution matrix [capacities x items]

    # solutions = [[0 for _ in range(item_count + 1)] for _ in range(capacity + 1)]
    solutions = np.zeros((capacity + 1, item_count))

    # next, Iterate over every item and place it in the knapsack if it fits, and increases the knsp value
    for j in range(item_count):
        for i in range(capacity + 1):
            current_item = items[j]
            # First, check if this item can fit in the kpsk
            if current_item.weight <= i:
                # add this item if valuble
                solutions[i][j] = max(
                    solutions[i][j - 1],
                    solutions[i - current_item.weight][j - 1] + current_item.value,
                )
            else:
                solutions[i][j] = solutions[i][j - 1]
        print(
            f"Currently placed {j} items in the knapsack. Total value is {solutions[i][j]}"
        )
    print(solutions)

    # Lastly retrieve the solutions.
    cap = capacity
    item_idx = item_count - 1
    variables = [0] * (item_count - 1)
    total_weight = 0
    while item_idx >= 0:
        if solutions[cap][item_idx] != solutions[cap][item_idx - 1]:
            # then the item was selected
            print(f"Added item {item_idx} to the knapsack.")
            variables[item_idx] = 1
            cap -= items[item_idx].weight
            total_weight += items[item_idx].weight
        item_idx -= 1
    chosen = [i for i in range(len(variables)) if variables[i] == 1]
    print(
        f"The resulting solution is items {chosen}, with total weight of {total_weight}."
    )
    print(f"There was {cap} available capacity in the knapsack")
    assert capacity - total_weight == cap
    "Error in the capacity calcs. Check the backtracking"

    return chosen


def solve_it(input_data):

    # Current method: ortools knapsack solver Branch and Bound
    # Methods to include in future:
    # ? GRASP

    # parse the input
    lines = input_data.split("\n")

    firstLine = lines[0].split()
    item_count = int(firstLine[0])
    capacity = int(firstLine[1])

    items = []

    for i in range(1, item_count + 1):
        line = lines[i]
        parts = line.split()
        items.append(
            Item(i - 1, int(parts[0]), int(parts[1]), int(parts[0]) / int(parts[1]))
        )

    # The dedicated ortools knapsack solver is very fast.
    solver = pywrapknapsack_solver.KnapsackSolver(
        pywrapknapsack_solver.KnapsackSolver.KNAPSACK_MULTIDIMENSION_BRANCH_AND_BOUND_SOLVER,
        "test",
    )
    values = []
    weights = []
    for i in range(item_count):
        values.append(items[i].value)
        weights.append(items[i].weight)

    weights = [weights]  # list because modified from bin-packing
    capacity = [capacity]  # list because modified from bin-packing

    solver.Init(values, weights, capacity)
    computed_value = solver.Solve()

    packed_items = [
        x for x in range(0, len(weights[0])) if solver.BestSolutionContains(x)
    ]
    packed_weights = [weights[0][i] for i in packed_items]
    packed_value = [values[i] for i in packed_items]

    # prepare the solution in the specified output format
    taken = [0] * item_count
    for i in range(len(packed_items)):
        taken[packed_items[i]] = 1

    output_data = str(sum(packed_value)) + " " + str(1) + "\n"
    output_data += " ".join(map(str, taken))
    # print(output_data)
    return output_data


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, "r") as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        file_location = Path.cwd() / "knapsack" / "data" / "ks_100_0"
        with open(file_location, "r") as input_data_file:
            input_data = input_data_file.read()
        print(solve_kpsk_dp(input_data))

        print(
            "This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/ks_4_0)"
        )


"""

Previous methods:
1. Value density greedy algorithm
from scipy import optimize


    # a trivial greedy algorithm for filling the knapsack
    # it takes items in-order until the knapsack is full
    value = 0
    weight = 0
    taken = [0] * len(items)
    items = sorted(items, key=lambda item: item[3], reverse=True)

    for item in items:
        if weight + item.weight <= capacity:
            taken[item.index] = 1
            value += item.value
            weight += item.weight

    value_t = 0
    weight_t = 0
    taken_t = [0] * len(items)
    items = sorted(items, key=lambda item: item[3], reverse=False)

    for item in items:
        if weight_t + item.weight <= capacity:
            taken_t[item.index] = 1
            value_t += item.value
            weight_t += item.weight

    if value_t > value:
        value = value_t
        taken = taken_t
        weight = weight_t

"""
