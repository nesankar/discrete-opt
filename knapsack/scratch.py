from collections import namedtuple

# multi comodity flo
commodity = namedtuple("comodity", ["source", "sink", "demand"])

G = np.zeros(nodes, nodes) # The values of this matrix are filled in with 1 if there is a path from i->j
caps = np.zeros(nodes, nodes) # The values of this matrix are the capacity on edge i->j
f = np.zeros(nodes, nodes, comodities) # The frac of flow of comodity c on every edge
costs = np.zeros(nodes, nodes) # The values of this matrix are the costs on edge i->j



c1 = sum([f[i, j, k.idx] for k in comodities]) <= caps[i, j]  # for all i, j pairs in G (list)
c2 = sum([f[k.source, i, k] for i in nodes]) = k.demand  # for all k (list)
c3 = sum([f[i, k.sink, k] for i in nodes]) = k.demand  # for all k (list)

# Mass balance at all nodes
c4 = sum([sum([f[j, i, k] - f[i, j, k] for i in nodes]) for j in nodes.subtract(k.source, k.sink)]) = 0  # for all k

obj = sum([sum([sum([f[i, j, k] * costs[i, j] for i in nodes]) for j in nodes]) for k in comodities])
