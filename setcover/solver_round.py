#!/usr/bin/python
# -*- coding: utf-8 -*-

# The MIT License (MIT)
#
# Copyright (c) 2014 Carleton Coffrin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from collections import namedtuple
from ortools.linear_solver import pywraplp
from scipy.optimize import fsolve
import numpy as np
import random


Set = namedtuple("Set", ['index', 'cost', 'items'])

#def solve_it(input_data):
    # Modify this code to run your optimization algorithm

    # parse the input
input_data=open('data/sc_1000_11').read()
lines = input_data.split('\n')

parts = lines[0].split()
item_count = int(parts[0])
set_count = int(parts[1])

n=item_count
gv=1/(4*n)
d=10
eq=2*np.exp(-d*np.log(n))
while eq>gv:
    d+=1
    eq=2*np.exp(-d*np.log(n))
    


sets = []
for i in range(1, set_count+1):
    parts = lines[i].split()
    sets.append(Set(i-1, float(parts[0]), set([int(val) for val in parts[1:]])))

# build a trivial solution
# pick add sets one-by-one until all the items are covered

solver = pywraplp.Solver('LinearRounding',
                       pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)

# Define the variable x -- = 1 if set[i] is chosen
x=[solver.NumVar(0, 1,'x%i' % i) for i in range(set_count)]
# Define the objective function. Minimize the cost of the sets chosen.
obj = solver.Objective()
for i in range(set_count):
    obj.SetCoefficient(x[i], sets[i].cost)
obj.SetMinimization()

# Define the constraints. 1 constraint per item -- mening that the sum of the
# sets chosen that conain the item is >=1
constraints=[]
for i in range(item_count):
    constraint_temp=solver.Constraint(1, solver.Infinity())
    for si in range(set_count):
        if i in sets[si].items:
            constraint_temp.SetCoefficient(x[si],1)
        else:
            constraint_temp.SetCoefficient(x[si],0)
    constraints.append(constraint_temp)
    

result_status = solver.Solve()
# The problem has an optimal solution.
print(result_status)
# assert result_status == pywraplp.Solver.OPTIMAL

# The solution looks legit (when using solvers other than
# GLOP_LINEAR_PROGRAMMING, verifying the solution is highly recommended!).
assert solver.VerifySolution(1e-7, True)

print('Number of variables =', solver.NumVariables())
print('Number of constraints =', solver.NumConstraints())

# The objective value of the solution.
print('Optimal objective value = %f' % solver.Objective().Value())
print()
# The value of each variable in the solution.
variable_list = x
sol=[0]*len(x)
frac=[0]*len(x)
for i in range(int(d*np.log(n))):
    for ii in range(len(sol)):
        variable=variable_list[ii]
        value=variable.solution_value()
        p_check=random.random()
        if value >= p_check:
            value_r=1
            sol[ii]=1
        else:
            value_r=0
            sol[ii]=0
        frac[ii]=value
        
    # print('%s = %d' % (variable.name(), variable.solution_value()))


# calculate the cost of the solution
obj = solver.Objective().Value()
solution=sol
obj=0
for i in range(len(solution)):
    obj += solution[i]*sets[i].cost
#
## prepare the solution in the specified output format
output_data = str(obj) + ' ' + str(0) + '\n'
output_data += ' '.join(map(str, solution))

#   return output_data
#
#
#import sys
#
#if __name__ == '__main__':
#    import sys
#    if len(sys.argv) > 1:
#        file_location = sys.argv[1].strip()
#        with open(file_location, 'r') as input_data_file:
#            input_data = input_data_file.read()
#        print(solve_it(input_data))
#    else:
#        print('This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/sc_6_1)')

