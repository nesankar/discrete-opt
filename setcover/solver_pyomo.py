#!/usr/bin/python
# -*- coding: utf-8 -*-

# The MIT License (MIT)
#
# Copyright (c) 2014 Carleton Coffrin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from collections import namedtuple
from pyomo.environ import *
import numpy as np
import random

Set = namedtuple("Set", ["index", "cost", "items"])


def solve_it(input_data):
    opt = 1

    # parse the input
    # input_data = open("setcover/data/sc_6_1").read()
    lines = input_data.split("\n")

    parts = lines[0].split()
    item_count = int(parts[0])
    set_count = int(parts[1])

    sets = []
    for i in range(1, set_count + 1):
        parts = lines[i].split()
        sets.append(Set(i - 1, float(parts[0]), set([int(val) for val in parts[1:]])))

    # Create the pyomo model
    model = ConcreteModel()

    model.x = Var(range(set_count), within=Binary)

    # Define the constraints each item must be within a chosen set, "covered"
    # For a number of (or list) of constraints, ConstraintList() is better
    model.item_covered = ConstraintList()
    for i in range(item_count):
        items = [0] * set_count
        for j in range(set_count):
            if i in sets[j].items:
                items[j] = 1
        model.item_covered.add(
            sum(model.x[ix] * items[ix] for ix in range(set_count)) >= 1
        )

    # Define the objective function
    # Minimize the number of sets chosen
    model.obj = Objective(
        expr=sum([model.x[i] for i in range(set_count)]), sense=minimize
    )

    # Solve the problem
    solver = SolverFactory("cbc")
    solver.solve(model)

    # Extract the solution
    obj = model.obj()
    solution = [int(model.x[i].value) for i in range(set_count)]

    # print("Optimal objective value = %d" % obj)
    ## prepare the solution in the specified output format
    output_data = str(obj) + " " + str(opt) + "\n"
    output_data += " ".join(map(str, solution))
    print(output_data)
    return output_data


import sys

if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, "r") as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        print(
            "This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/sc_6_1)"
        )
