#!/usr/bin/python
# -*- coding: utf-8 -*-

# The MIT License (MIT)
#
# Copyright (c) 2014 Carleton Coffrin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from collections import namedtuple
from ortools.linear_solver import pywraplp
import numpy as np
import random
import sys


import cplex as cpx
from docplex.cp.model import CpoModel

# sys.path.append("CPLEX_Studio_Community129/python/3.6/x86-64_osx")


# doc @ http://ibmdecisionoptimization.github.io/docplex-doc/cp/docplex.cp.model.py.html


Set = namedtuple("Set", ["index", "cost", "items"])


# def solve_it(input_data):
# Modify this code to run your optimization algorithm

# parse the input
input_data = open("setcover/data/sc_6_1").read()
lines = input_data.split("\n")

parts = lines[0].split()
item_count = int(parts[0])
set_count = int(parts[1])

sets = []
for i in range(1, set_count + 1):
    parts = lines[i].split()
    sets.append(Set(i - 1, float(parts[0]), set([int(val) for val in parts[1:]])))

url = None
key = None

model = CpoModel()
if set_count > 1:
    opt = 1

    x = model.integer_var_list(set_count, 0, 1, "open")

    # Define the objective function. Minimize the cost of the sets chosen.
    num_sets_chosen = model.scal_prod(x, [1 for s in x])
    model.add(model.minimize(num_sets_chosen))

    # Define the constraints.
    # 1 constraint per item -- mening that the sum of the
    # sets chosen that contain the item is >=1
    for i in range(item_count):
        item_in_set = [0] * set_count
        for s in range(set_count):
            if i in sets[s].items:
                item_in_set[s] = 1
        model.add(model.scal_prod(x, item_in_set) >= 1)

    print("\nSolving model....")

    sol = model.solve()

    result_status = sol.get_solve_status()
    obj = sol.get_objective_values()
    solution = sol.get_solution()
    values = sol.get_all_var_solutions()
    opt = sol.is_solution_optimal()

    # The problem has an optimal solution.

    # The value of each variable in the solution.
    """
    variable_list = x
    sol = []
    for variable in variable_list:
        sol.append(int(variable.solution_value()))
        # print('%s = %d' % (variable.name(), variable.solution_value()))

    # calculate the cost of the solution
    obj = solver.Objective().Value()
    solution = sol
    obj = 0
    for i in range(len(solution)):
        obj += solution[i] * sets[i].cost
#
## prepare the solution in the specified output format
output_data = str(obj) + " " + str(opt) + "\n"
output_data += " ".join(map(str, solution))
"""

# return output_data


import sys

if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, "r") as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        print(
            "This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/sc_6_1)"
        )
