import random
import numpy as np
import math

"""

Using a greedy algorithm for facility location.
Basis for the algorithm:
  1. Define a local neighboorhood distance, N. --> Perhaps a function
     of the open cost and mean facility distances, and current facility capacity ?
     CURRENTLY: Using the n facilities which "fulfill" a facility's capacity.
  2. Assign all customers to a facility within distance N.

  While all customers not assigned:
    3. Open the facility with least cost/customer to open.
    4. move customers from the un-assigned set to the assigned set.
    5. Perform a local search (random) for all customers
       assigned that could be assigned to a different facility.
    6. Iterate

  If any customers remain, place them at closest feasible open facility, or open a facility if required
"""
# The cost function between facility and customer.
def length(point1, point2):
    return math.sqrt((point1.x - point2.x) ** 2 + (point1.y - point2.y) ** 2)


def objective(neighboorhood, facilities, customers, assigned_customers, current_fac):
    # Calculate what facilities were used in the solution

    # Calculate the solution cost.
    # 1. cost to open facility
    sol_cost = facilities[current_fac].setup_cost

    # 2. cost to serve customers
    served_customers = set(neighboorhood).difference(assigned_customers)
    for customer in customers:
        if customer.index in served_customers:
            sol_cost += length(customer.location, facilities[current_fac].location)

    return sol_cost


def ls_objective(solution, facilities, customers):
    used = [0] * len(facilities)
    for facility_index in solution:
        used[facility_index] = 1

    # Record the demand served by a facility
    served_vol = [0] * len(facilities)

    # Calculate the solution cost.
    sol_cost = sum([f.setup_cost * used[f.index] for f in facilities])
    for customer in customers:
        sol_cost += length(
            customer.location, facilities[solution[customer.index]].location
        )
        served_vol[solution[customer.index]] += customer.demand
    violation = 0

    # Compute the amount of demand served by a facility exceeding the facility's capacity
    for i in range(len(facilities)):
        if served_vol[i] > facilities[i].capacity:
            violation += served_vol[i] - facilities[i].capacity
    return (sol_cost, violation)


def capacity_check(facility, customers, c_serving, c_new):
    served_customers = set(c_serving)
    dem = 0
    for customer in customers:
        if customer.index in served_customers:
            dem += customer.demand
    dem += c_new.demand
    return facility.capacity - dem


def facility_heuristic(
    n_facilities, n_customers, facilities, customers, params=[100, 100]
):
    # Currently expiramenting with the neighboorhood size -- UNUSED. Ideally, N is a function
    # of the average(?) distances between the facilities and the customers, and the
    # cost to open a facility.
    # CURRENTLY: neighboorhood is represented as the nearest customers to a facility that
    # fulfill the facility's capacity.
    N = [params[0]] * n_facilities
    ls_iterations = params[1]

    # Pre-compute the distances:
    cost_mat = []
    for i in range(n_facilities):
        travel = [0] * n_customers
        for j in range(n_customers):
            dist = length(facilities[i].location, customers[j].location)
            travel[j] = dist
        cost_mat.append(travel)

    # Compute the neighboorhood sets
    demand_servicable = 0.8
    fac_2_customers = {}
    customers_2_fac = {}
    for i in range(n_facilities):
        j = 0
        neighboors = np.argsort(cost_mat[i])
        delivering = 0
        while (
            delivering < facilities[i].capacity * demand_servicable and j < n_customers
        ):
            # Check if the current facility in dict
            # Then map the current customer to the facility key
            if (
                delivering + customers[neighboors[j]].demand
                <= facilities[i].capacity * demand_servicable
            ):
                if j == 0:
                    fac_2_customers[i] = [neighboors[j]]
                else:
                    fac_2_customers[i].append(neighboors[j])
                # Check if the current customer is in the dict
                # Then map the current facility to the current customer key
                if neighboors[j] in customers_2_fac:
                    customers_2_fac[neighboors[j]].append(i)
                else:
                    customers_2_fac[neighboors[j]] = [i]

                # keep track of the current demand served
                delivering += customers[neighboors[j]].demand
            j += 1

    # Define the customer assigned and unasigned sets
    c_assigned = set()
    c_unassigned = set(range(n_customers))
    f_solution = {}
    print("Begining the greedy search.")
    count = 0
    # Compute the cost of the initial facility to open per customer
    values = [float("inf")] * n_facilities
    for f in range(n_facilities):
        cluster_customers = fac_2_customers[f]
        cost = objective(cluster_customers, facilities, customers, c_assigned, f)
        served = list(set(cluster_customers).difference(c_assigned))
        if len(served) > 0:
            value_temp = cost / len(served)
        else:
            value_temp = float("inf")
        values[f] = value_temp
    bst_fc = np.argmin(values)
    newly_assigned = list(set(fac_2_customers[bst_fc]).difference(c_assigned))
    f_solution[bst_fc] = newly_assigned

    for c in newly_assigned:
        c_assigned.add(c)
        c_unassigned.remove(c)
    count += 1
    print(
        "Opened the %d-th facility. Assigned %d percent of the customers."
        % (count, (len(c_assigned) / n_customers) * 100)
    )
    # Iterate while any customers have not yet been assigned
    while c_unassigned:
        values = [float("inf")] * n_facilities
        for f in range(n_facilities):
            if f not in f_solution:
                cluster_customers = fac_2_customers[f]
                # This objective computes facility costs independently. Should be investigated more for validity.
                cost = objective(
                    cluster_customers, facilities, customers, c_assigned, f
                )
                served = list(set(cluster_customers).difference(c_assigned))
                if len(served) > 0:
                    value_temp = cost / len(served)
                else:
                    value_temp = float("inf")
                values[f] = value_temp
        bst_fc = np.argmin(values)
        newly_assigned = list(set(fac_2_customers[bst_fc]).difference(c_assigned))
        if len(newly_assigned) == 0:
            # Stop opening if no new customers are assigned.
            break
        f_solution[bst_fc] = newly_assigned

        for c in newly_assigned:
            c_assigned.add(c)
            c_unassigned.remove(c)
        count += 1
        print(
            "Opened the %d-th facility. Assigned %d percent of the customers."
            % (count, (len(c_assigned) / n_customers) * 100)
        )

    # Assign any unassigned customers to the closes feasible facility
    for c in list(c_unassigned):
        d_t = [0] * n_facilities
        for f in range(n_facilities):
            d_t[f] = length(facilities[f].location, customers[c].location)
        near_f = np.argsort(d_t)
        # First check open facilities
        matched = False
        temp_val = []
        for i in range(len(near_f)):
            if near_f[i] in f_solution:
                viol = capacity_check(
                    facilities[near_f[i]],
                    customers,
                    f_solution[near_f[i]],
                    customers[c],
                )
                cst = length(customers[c].location, facilities[near_f[i]].location)
                if viol > 0:
                    temp_val.append([cst, near_f[i]])
        if len(temp_val) > 0:
            temp_val.sort(key=lambda x: x[0])
            matched = True
            f_solution[temp_val[0][1]].append(c)
            # Updated the options dictionaries for the local search procedure.
            if near_f[i] in customers_2_fac:
                fac_2_customers[temp_val[0][1]].append(c)
            else:
                fac_2_customers[temp_val[0][1]] = [c]
            if c in customers_2_fac:
                customers_2_fac[c].append(temp_val[0][1])
            else:
                customers_2_fac[c] = [temp_val[0][1]]

        # If not assignable to an open facility
        if matched == False:
            temp_val = []
            viols = []
            for i in range(len(near_f)):
                if near_f[i] not in f_solution:
                    viol = capacity_check(
                        facilities[near_f[i]], customers, [], customers[c]
                    )
                    cst = facilities[near_f[i]].setup_cost + length(
                        customers[c].location, facilities[near_f[i]].location
                    )
                    if viol > 0:
                        temp_val.append([cst, near_f[i]])
            temp_val.sort(key=lambda x: x[0])
            matched = True
            f_solution[temp_val[0][1]] = [c]
            # Updated the options dictionaries for the local search procedure.
            if near_f[i] in customers_2_fac:
                fac_2_customers[temp_val[0][1]].append(c)
            else:
                fac_2_customers[temp_val[0][1]] = [c]
            if c in customers_2_fac:
                customers_2_fac[c].append(temp_val[0][1])
            else:
                customers_2_fac[c] = [temp_val[0][1]]

    solution = [0] * n_customers
    for fac, customers_assigned in f_solution.items():
        for c in customers_assigned:
            solution[c] = fac

    print("Greedy method complete.")
    print("Entering local search.")
    # Perform a simple local search procedure
    cost_min, violation = ls_objective(solution, facilities, customers)
    cost_o = cost_min
    for i in range(ls_iterations):
        sol_temp = solution.copy()
        customer_1 = random.randint(0, n_customers - 1)
        fac_1 = solution[customer_1]
        facility_options = customers_2_fac[customer_1]
        rand_loc_1 = random.randint(0, len(facility_options) - 1)
        fac_2 = facility_options[rand_loc_1]
        customer_2_options = fac_2_customers[fac_2]
        rand_loc_2 = random.randint(0, len(customer_2_options) - 1)
        customer_2 = customer_2_options[rand_loc_2]
        sol_temp[customer_2] = fac_1
        sol_temp[customer_1] = fac_2
        cost, violation = ls_objective(sol_temp, facilities, customers)
        if cost < cost_min and violation == 0:
            solution = sol_temp
            cost_min = cost
        if i % 500 == 0:
            print(
                "Completed %d swaps. Absolute Solution improvement of %f. Relative improvement of %.2f percent"
                % (i, cost_o - cost_min, ((cost_o - cost_min) / cost_o) * 100)
            )

    print("Greedy method complete")
    return solution
