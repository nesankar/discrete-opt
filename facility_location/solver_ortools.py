#!/usr/bin/python
# -*- coding: utf-8 -*-

from collections import namedtuple
import math
import numpy as np
from ortools.linear_solver import pywraplp
import os

"""
Solving the facility location problem using COIN OR branch and cut.
   Other options include:
   .GLPK_MIXED_INTEGER_PROGRAMMING
   .CBC_MIXED_INTEGER_PROGRAMMING <-- Only this one works without building source code?
   .SCIP_MIXED_INTEGER_PROGRAMMING
   .GUROBI_MIXED_INTEGER_PROGRAMMING
   .CPLEX_MIXED_INTEGER_PROGRAMMING

Things to look in to:
https://github.com/google/or-tools/blob/stable/examples/contrib/p_median.py
solver.Sum() function/method
solver.Add() constraint usage

"""

Point = namedtuple("Point", ["x", "y"])
Facility = namedtuple("Facility", ["index", "setup_cost", "capacity", "location"])
Customer = namedtuple("Customer", ["index", "demand", "location"])


def length(point1, point2):
    return math.sqrt((point1.x - point2.x) ** 2 + (point1.y - point2.y) ** 2)


# def solve_it(input_data):

# For debugging:
input_data = open("Hw_6/data/fl_100_7").read()
# parse the input
lines = input_data.split("\n")

parts = lines[0].split()
facility_count = int(parts[0])
customer_count = int(parts[1])

# M is a term for the MIP formulation shown here:
#  https://en.wikipedia.org/wiki/Facility_location_problem
M = customer_count / 4
facilities = []
for i in range(1, facility_count + 1):
    parts = lines[i].split()
    facilities.append(
        Facility(
            i - 1,
            float(parts[0]),
            int(parts[1]),
            Point(float(parts[2]), float(parts[3])),
        )
    )

customers = []
for i in range(facility_count + 1, facility_count + 1 + customer_count):
    parts = lines[i].split()
    customers.append(
        Customer(
            i - 1 - facility_count,
            int(parts[0]),
            Point(float(parts[1]), float(parts[2])),
        )
    )

cost_mat = []
for i in range(facility_count):
    travel = [0] * customer_count
    for j in range(customer_count):
        dist = length(facilities[i].location, customers[j].location)
        travel[j] = dist
    cost_mat.append(travel)

solver = pywraplp.Solver(
    "SolveIntegerProblem", pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING
)

# Create a 2-D decision variable matrix
# rows are the facilities, indices of the rows are customers
# An entry of [1,3] == 1 means facility 1 serves customer 3
fac_var = []
for f in range(facility_count):
    # customers_assigned=[]
    var = [solver.IntVar(0, 1, "f%i-c%i" % (f, i)) for i in range(customer_count)]
    fac_var.append(var)

# Create the facility capacity constraint:
cap_constraints = []
for i in range(facility_count):
    constraint_temp = solver.Constraint(-solver.Infinity(), facilities[i].capacity)
    for j in range(customer_count):
        constraint_temp.SetCoefficient(fac_var[i][j], customers[j].demand)
    cap_constraints.append(constraint_temp)

# Create the variable dependent on facilities opened
open_var = [solver.BoolVar("open%d" % i) for i in range(facility_count)]

# Begin the opening constraints
open_constraints = []
for i in range(facility_count):
    open_constraint_temp = solver.Constraint(-solver.Infinity(), 0)
    open_constraint_temp.SetCoefficient(open_var[i], -M)
    open_constraints.append(open_constraint_temp)

# Create the service constraints, each customer must be served
for j in range(customer_count):
    constraint_temp = solver.Constraint(1, 1)
    for i in range(facility_count):
        if cost_mat[i][j] < D:
            constraint_temp.SetCoefficient(fac_var[i][j], 1)
        open_constraints[i].SetCoefficient(fac_var[i][j], 1)
    service_constraints.append(constraint_temp)

# Create the objective function
obj = solver.Objective()
for i in range(facility_count):
    obj.SetCoefficient(open_var[i], facilities[i].setup_cost)
    for j in range(customer_count):
        obj.SetCoefficient(fac_var[i][j], cost_mat[i][j])

obj.SetMinimization()

print("Solving the problem:")
print("Number of variables: %d" % solver.NumVariables())
print("Number of constraints: %d" % solver.NumConstraints())
result_status = solver.Solve()
print(result_status)  # A value of 2 is infeasible
# The problem has an optimal solution.
assert (
    result_status == pywraplp.Solver.OPTIMAL
    or result_status == pywraplp.Solver.FEASIBLE
)

# The solution looks legit (when using solvers other than
# GLOP_LINEAR_PROGRAMMING, verifying the solution is highly recommended!).
assert solver.VerifySolution(1e-7, True)

print("Number of variables =", solver.NumVariables())
print("Number of constraints =", solver.NumConstraints())

# The objective value of the solution.
print("Optimal objective value = %d" % solver.Objective().Value())
print()
# The value of each variable in the solution.
sol = [0] * customer_count
create_mat = []
opened = [0] * facility_count
for i in range(facility_count):
    if open_var[i].solution_value() == 1:
        opened[i] = 1
    t = []
    for j in range(customer_count):
        if fac_var[i][j].solution_value() == 1:
            sol[j] = i
            t.append(1)
        else:
            t.append(0)
    create_mat.append(t)
# calculate the cost of the solution
obj_value = solver.Objective().Value()
solution = sol

used = [0] * len(facilities)
for facility_index in solution:
    used[facility_index] = 1

# calculate the cost of the solution
obj_calc = sum([f.setup_cost * used[f.index] for f in facilities])
for customer in customers:
    obj_calc += length(customer.location, facilities[solution[customer.index]].location)

# prepare the solution in the specified output format
output_data = "%.2f" % obj_calc + " " + str(0) + "\n"
output_data += " ".join(map(str, solution))
print(output_data)
# return output_data


import sys

if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, "r") as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        print(
            "This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/fl_16_2)"
        )
