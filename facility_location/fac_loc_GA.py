import random
import numpy as np
import math

"""

Using a GA  to try to place facilities for large instances of the facility location problem.

"""
# The cost function between facility and customer.
def length(point1, point2):
    return math.sqrt((point1.x - point2.x) ** 2 + (point1.y - point2.y) ** 2)


def initialize_pop(n_customers, n_facilities, pop_size):
    out_pop = []
    for s in range(pop_size):
        temp_sol = [0] * n_customers
        for c in range(n_customers):
            temp_sol[c] = random.randint(0, n_facilities - 1)
        out_pop.append(temp_sol)
    return out_pop


def objective(solution, facilities, customers):
    # Calculate what facilities were used in the solution

    used = [0] * len(facilities)
    for facility_index in solution:
        used[facility_index] = 1

    # Record the demand served by a facility
    served_vol = [0] * len(facilities)

    # Calculate the solution cost.
    sol_cost = sum([f.setup_cost * used[f.index] for f in facilities])
    for customer in customers:
        sol_cost += length(
            customer.location, facilities[solution[customer.index]].location
        )
        served_vol[solution[customer.index]] += customer.demand
    violation = 0

    # Compute the amount of demand served by a facility exceeding the facilites capacity
    for i in range(len(facilities)):
        if served_vol[i] > facilities[i].capacity:
            violation += served_vol[i] - facilities[i].capacity
    return (sol_cost, violation)


def tournament(scores, players):
    best = float("inf")
    for player in players:
        if scores[player] < best:
            winner = player
            best = scores[player]
    return winner


def single_point_cx(mother, father):
    child = [0] * len(mother)

    # Determine the crossover point. Takes at least 1 value from father
    c_point = random.randint(1, len(mother) - 2)
    for i in range(len(mother)):
        if i < c_point:
            child[i] == mother[i]
        else:
            child[i] = father[i]
    return child


def facility_heuristic(
    n_facilities,
    n_customers,
    facilities,
    customers,
    params=[100, 500, 0.8, 0.1, 3, 10000],
):

    # define the GA params
    pop_size = GA_params[0]
    generations = GA_params[1]
    p_cross = GA_params[2]
    p_mut = GA_params[3]
    n_players = GA_params[4]
    penalty = GA_params[5]

    # initialize a population
    population = initialize_pop(n_customers, n_facilities, pop_size)
    best_sol = float("inf")

    # Iterate over the number of generations
    for g in range(1, generations):

        # Evaluate the population
        fitness = []
        for sol in population:
            score_temp = objective(sol, facilities, customers)
            score = score_temp[0] + score_temp[1] * penalty
            fitness.append(score)

        # Select offspring from the population
        parents = []
        for i in range(len(population)):
            t1 = set()
            t2 = set()
            while len(t1) < n_players:
                t1.add(random.randint(0, len(population) - 1))
            while len(t2) < n_players:
                t2.add(random.randint(0, len(population) - 1))
            p1 = tournament(fitness, t1)
            p2 = tournament(fitness, t2)
            parents.append([p1, p2])

        # Apply crossover to the population
        offspring = []
        for pair in parents:
            cross_check = random.random()
            if cross_check < p_cross:
                child = single_point_cx(
                    population[pair[0]].copy(), population[pair[1]].copy()
                )
                offspring.append(child)
            else:  # Default to taking the mother
                offspring.append(population[pair[0]])

        # Apply mutation to offspring
        for i in range(len(offspring)):
            mut_check = random.random()
            if mut_check < p_mut:
                # mutate
                child = offspring[i].copy()
                mut_ind = random.randint(0, len(child) - 1)
                child[mut_ind] = random.randint(0, n_facilities - 1)
                offspring[i] = child

        # introduce elitism of 1
        if g > 1:
            offspring[0] = solution

        # Compute the current best
        if min(fitness) < best_sol:
            best_sol = min(fitness)
            solution = population[np.argmin(fitness)]
        if g % 30 == 0:
            print("\nCompleted Generation #%d\n" % g)
            print("\nBest objective so far is: %f\n" % best_sol)
        population = offspring

    print("GA complete")
    return solution
