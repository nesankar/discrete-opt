#!/usr/bin/python
# -*- coding: utf-8 -*-

from collections import namedtuple
import math
import numpy as np
import pyomo.environ as pyomo
import os

"""
Solving the facility location problem using COIN OR branch and cut with pyomo.

"""

Point = namedtuple("Point", ["x", "y"])
Facility = namedtuple("Facility", ["index", "setup_cost", "capacity", "location"])
Customer = namedtuple("Customer", ["index", "demand", "location"])


def length(point1, point2):
    return math.sqrt((point1.x - point2.x) ** 2 + (point1.y - point2.y) ** 2)


# def solve_it(input_data):

# For debugging:
input_data = open("Hw_6/data/fl_25_2").read()

# parse the input
lines = input_data.split("\n")

parts = lines[0].split()
facility_count = int(parts[0])
customer_count = int(parts[1])

# M is a term for the MIP formulation shown here:
#  https://en.wikipedia.org/wiki/Facility_location_problem
M = customer_count / 4
facilities = []
for i in range(1, facility_count + 1):
    parts = lines[i].split()
    facilities.append(
        Facility(
            i - 1,
            float(parts[0]),
            int(parts[1]),
            Point(float(parts[2]), float(parts[3])),
        )
    )

customers = []
for i in range(facility_count + 1, facility_count + 1 + customer_count):
    parts = lines[i].split()
    customers.append(
        Customer(
            i - 1 - facility_count,
            int(parts[0]),
            Point(float(parts[1]), float(parts[2])),
        )
    )

cost_mat = []
for i in range(facility_count):
    travel = [0] * customer_count
    for j in range(customer_count):
        dist = length(facilities[i].location, customers[j].location)
        travel[j] = dist
    cost_mat.append(travel)

model = pyomo.ConcreteModel()

model.facilities = range(facility_count)
model.customers = range(customer_count)

# Create a 2-D decision variable matrix
# rows are the facilities, indices of the rows are customers
# An entry of [1,3] == 1 means facility 1 serves customer 3
model.service_var = pyomo.Var(model.facilities, model.customers, within=pyomo.Binary)

# Create the variable dependent on facilities opened
model.open_var = pyomo.Var(model.facilities, within=pyomo.Binary)

# Create the facility capacity constraint:
model.cap_constraints = pyomo.ConstraintList()
for i in model.facilities:
    model.cap_constraints.add(
        sum([model.service_var[i, j] * customers[j].demand for j in model.customers])
        <= facilities[i].capacity
    )

# Begin the opening constraints
model.open_constraints = pyomo.ConstraintList()
for i in model.facilities:
    model.open_constraints.add(
        model.open_var[i] * M >= sum([model.service_var[i, j] for j in model.customers])
    )

# Service constraint, a customer can be solved by only 1 facility
model.service_constraints = pyomo.ConstraintList()
for j in model.customers:
    model.service_constraints.add(
        sum([model.service_var[i, j] for i in model.facilities]) == 1
    )

# Create the objective function
model.obj = pyomo.Objective(
    expr=sum([model.open_var[i] * facilities[i].setup_cost for i in model.facilities])
    + sum(
        [
            sum([model.service_var[i, j] * cost_mat[i][j] for i in model.facilities])
            for j in model.customers
        ]
    ),
    sense=pyomo.minimize,
)

solver = pyomo.SolverFactory("cbc")
print("Solving the model")
solver.solve(model, tee=True)
print("Model is solved")

# The value of each variable in the solution.
solution = [0] * customer_count
for i in model.customers:
    for j in model.facilities:
        if model.service_var[j, i] == 1:
            solution[i] = j
            break

# calculate the cost of the solution
used = [0] * len(facilities)
for facility_index in solution:
    used[facility_index] = 1

# calculate the cost of the solution
obj = sum([f.setup_cost * used[f.index] for f in facilities])
for customer in customers:
    obj += length(customer.location, facilities[solution[customer.index]].location)

obj_calc = model.obj()
print(obj_calc)

# prepare the solution in the specified output format
output_data = "%.2f" % obj + " " + str(0) + "\n"
output_data += " ".join(map(str, solution))
print(output_data)
# return output_data


import sys

if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, "r") as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        print(
            "This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/fl_16_2)"
        )
