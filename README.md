# Discrete Opt Solvers

A number of python programs written for solving knapsack, the TSP, facility location, graph coloring, and set cover.

Problems insatnces are provided from the coursera course Discrete Optimization.
https://www.coursera.org/learn/discrete-optimization/home/welcome

The packages used so far include:
    ortools,
    pyomo
